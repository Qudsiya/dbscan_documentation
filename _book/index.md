--- 
title: "Spatial Clustering with DBSCAN"
author: "Qudsiya Brey"
date: "2024-06-07"
site: bookdown::bookdown_site
documentclass: book
bibliography: [book.bib, packages.bib]
# url: your book url like https://bookdown.org/yihui/bookdown
# cover-image: path to the social sharing image like images/cover.jpg
description: |
  This is a minimal example of using the bookdown package to write a book.
  The HTML output format for this example is bookdown::bs4_book,
  set in the _output.yml file.
biblio-style: apalike
csl: chicago-fullnote-bibliography.csl
---

# Introduction {-}

Density-based spatial clustering of applications with noise (DBSCAN) is a non-parametric algorithm. Given the latitudes and longitudes of points in some space, it groups together points that are closely packed together. The distance is calculated between points using the haversine formula. The algorithm marks points that lie alone in low-density regions as outliers.

## Approach {-}
1. Calculate the pairwise haversine distance
2. Find optimal value for `eps` using KNN distance plot
3. Perform DBSCAN clustering
4. Map cluster groups for visualisation

## Requirements {-}


``` r
library(tidyverse)
library(readr)
library(dplyr)
library(geosphere) 
library(dbscan)
library(ggplot2)
library(colorspace)
library(leaflet)
```



