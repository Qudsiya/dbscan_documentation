# Haversine distance {-}

The haversine formula determines the great-circle distance between two points on a sphere given their longitudes and latitudes. The haversine calculation requires coordinate pairs to be specified in radians and not degrees.

The haversine formula between two points is defined by:

$$
D(a,b) = 2\arcsin \left( \sqrt{ \sin^2 \left( \frac{a_i - b_i}{2} \right) + \cos(a_i) \cos(b_i) \sin^2 \left( \frac{a_j - b_j}{2} \right) } \right)
$$ 

where `i` denotes latitude and `j` denotes longitude coordinates in radians.

## Implementation in R {-}

To calculate the pairwise haversine distance for points in a data set, the coordinates are placed in a matrix and the `distm()` function is used and the `fun` argument is set to `distHaversine`. The `distm()` function automatically converts the coordinates to radians so this does not have to be done manually. The resulting distances are in meters and can be converted to kilometers by dividing by 1000. The pairwise distances are stored in a *nxn* matrix where *n* denotes the number of points in the data set.


``` r
# Create a matrix of coordinates
  coords <- as.matrix(data_clean[, c("lon", "lat")])
  
  # Calculate the pairwise haversine distance matrix
  dist_matrix <- distm(coords, fun = distHaversine)/1000  # convert distances to km
```
