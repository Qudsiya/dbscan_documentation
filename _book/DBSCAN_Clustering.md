# Clustering using DBSCAN {-}

DBSCAN takes two parameters: `eps` and `minPts` which work together to define “density”. The algorithm does not require the number of clusters to be specified. 

- `eps` defines the maximum distance between two points for them to be considered in the same cluster. 
- `minPts`is the minimum number of points (a threshold) required to form a dense region (cluster).

To grasp the mechanics of the DBSCAN algorithm one must understand three important terms: **core-points**, **reachability** and **density-connected**.  


### Core-points {-}
A point *P* is a core-point if at least `minPts` are within distance `eps` of *P* (including point *P* itself). Points can only be *reached* from core-points.

### Reachability {-}
All points which are not reachable from any other point are called “noise” or “outliers”. Reachability is not a symmetric relation. Only core points can reach non-core points. The opposite is not true. A point *R* is reachable from point *P* if there is a path *P → Q → … → R* where each intermediate point is directly reachable from the preceding point (*P* and all other points along the path are core points with the possible exception of *R*).

### Density-connected {-}
Nothing can be reached from non-core points. Therefore, a further notion of connectedness is needed to formally define the extent of the clusters found by DBSCAN. Density-connectedness is symmetric. *P* is density-connected to *Q* and *Q* is density-connected to *P* only if there is a core point *O* that reaches both *Q* and *P*.
![overview](C:/Users/qudsi/OneDrive/Documents/DBSCAN/DBSCAN_Documentation/DBSCAN_overview.png)

A cluster must satisfy two properties:

1. All points within the cluster are mutually density-connected.
2. If a point is density-reachable from some core point of the cluster, it is part of the cluster as well.


## Choosing a value for `eps` {-}
Plotting the sorted distances against the nearest-neighbours can assist in selecting a suitable epsilon value for DBSCAN, leading to better clustering results. The "elbow" in the plot is where the distances start to increase more steeply. This point suggests a good epsilon value. In the example below using *k=3* nearest-neighbours, 0.4km suggests to be a good value. A rule-of-thumb is to choose $k = minPts - 1$
![knnPlot](C:/Users/qudsi/OneDrive/Documents/DBSCAN/DBSCAN_Documentation/knnPlot.png)
The plot can be generated in R using the `kNNdistplot()` function:

``` r
# Determine a good value for eps using kNNdistplot - look for 'elbow'
kNNdistplot(data_clean[c("lon", "lat")], k = 3)
```

## DBSCAN in R {-}
Once the values for `eps` and `minPts` is chosen, clustering can be done using the `dbscan()` function. Outliers will be assigned to cluster 0.


``` r
# Perform DBSCAN clustering
db <- dbscan(dist_matrix, eps = eps, minPts = minPts)
```
